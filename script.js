//ESTABLECE DE MOVIDA EL CÁLCULO A MOSTRAR Y LO MUESTRA
let resultado;

function calculaCaptcha() {
    let parrafo = document.querySelector("#calculo");
    let valor1 = Math.floor(Math.random()*10 + 0.5);
    let valor2 = Math.floor(Math.random() * 10 + 0.5);
    resultado = valor1 + valor2;
    console.log(valor1, " - ", valor2);
    parrafo.innerHTML = `${valor1} + ${valor2} = ?*`;
}

function validaNombre() {
    let nombre = document.querySelector("#nombre").value;
    if (nombre.trim() == "") {
        alert("Falta completar nombre completo");
        document.querySelector("#nombre").focus();
        return false;   //SI NO INGRESÓ NADA RETORNAMOS FALSE
    } else {
        return true;    //SI NO TRUE
    }
}

function validar() {
    //VALIDA NOMBRE
    // let nombre = document.querySelector("#nombre").value;
    // if (nombre.trim() == "") {
    //     alert("el nombre está vacío");
    //     document.querySelector("#nombre").focus();
    //     return;
    // }
    //COMO EJEMPLO, SI QUISIÉRAMOS PONER LA VALIDACIÓN EN UNA FUNCIÓN,
    //PREGUNTARÍAMOS SI ANDUVO BIEN O NO PARA SALIR DE VALIDAR(), 
    //COMO ESTÁ A CONTINUACIÓN.

    if (!validaNombre()) return;    //SI VALIDANOMBRE() RETORNÓ FALSE SALE DE VALIDAR()

    //Y ASÍ PODRÍAMOS HACER CON LOS DEMÁS ELEMENTOS A VALIDAR...
    //NO OBSTANTE, DEJÉ TODO JUNTO PARA QUE RESULTE MÁS FACIL DE ENTENDER

    //VALIDA EMAIL
    let email = document.querySelector("#email").value;
    const validRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //EXPRESIONES REGULARES
    if (!email.match(validRegEx)) {
        alert("El email está incompleto");
        document.querySelector("#email").focus();
        return;
    }
    //VALIDA CAPTCHA
    let inputResul = document.querySelector("#captcha").value;
    if (inputResul.trim() === "") {
        alert("Debe completar el cálculo");
        document.querySelector("#captcha").focus();
        return;
    }
    if (isNaN(inputResul)) {
        alert("No se ingresó un número");
        document.querySelector("#captcha").focus();
        return;
    }
    inputResul = parseFloat(inputResul);

    if (resultado !== inputResul) {
        alert("Resultado incorrecto");
        document.querySelector("#captcha").focus();
        return;
    }


    alert("Suscripción realizada con éxito");
}

function main() {
    calculaCaptcha();
}

main(); 